# coding=utf8
import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np
import random
import math
import time

STR_DELIMITER = ","
POS_REVIWER, POS_NUM_ARTICLE = 0, 1


class Individual(object):
    """
        Classe responsavel por armazenar a estrutura de saida do algoritmo genetico
        Esta classe contera apenas o valor de 1 cromossomo, junto com suas funcoes basicas
        Como multacao, crossover e funcao de avaliacao e integridade individual
    """

    # Variaveis globais que indicam em qual posicao da tupla esta a posicao do revisor e a posicao do numero de artigos

    def __init__(self, chromosome=[], matrix=None):
        # Esta variavel eh responsavel por me indicar o valor de avaliacao que este cromossomo tera
        # Quanto menor o valor do score, mais adaptado o cromossomo esta
        self.score = 0
        # Sera usado para avaliar o score do cromossomo
        self.Matrix = matrix
        # Pega o numero de artigos passado pela matriz
        self.numArticles = self.Matrix.shape[1] - 1
        
        # Variavel usada para saber a quantidade de artigos que cada revisor pode receber
        self.reviewers = None
        # Variavel criada para armazenar uma tupla de (id_revisor, num_artigos) que ira servir para
        # Indicar quais revisores estao disponiveis para ser usados como escolha
        self.availableReviewer = []
        
        # Caso a minha lista nao tenha revisores disponiveis por meio da inicializacao
        # Inicializo o mesmo por meio da funcao reviewers
        if (len(self.availableReviewer) == 0):
            self.__reviewers()
        
        # Caso seja passado algum parametro na classe inicia com este paramentro
        if (any(chromosome)):
            # Inicia o valor do cromossomo ou com o valor de entrada, ou na funcao de criacao
            self.chromosome = chromosome
        # Caso contratro cria uma nova lista
        else:
            self.chromosome =  self.__makeChromosome()

        # Faz o calculo de revisores disponivis apos a atribuicao dos cromossomos
        self.__available()

    # Funcao auxiliar linear para pegar a posicao da minha tupla do revisor esta alocado
    def getPosReviwer(self, x):
        pos = -1
        for i in xrange(len(self.availableReviewer)):
            if (self.availableReviewer[i][0] == x):
                pos = i
                break
        return pos

    # Calcula os revisores disponiveis
    # Apos calcular o numero de artigos que os revires tem disponiveis
    # Exclui os revisores que ficaram com os artigos restantes zerados
    def __available(self):
        # Cria um vetor para deletar os revisores que nao podem fazer revisoes de artigos
        vetDel = []

        # Cara cada elemento do cromossomo
        for i in self.chromosome:
            # Pego a posicao da tupla de revisor disponivel
            x = self.getPosReviwer(i)
            # Caso o mesmo se encontra no vetor, epenas diminui o numero de artigos que o dado revisor pode revisar
            if (x != -1):
                self.availableReviewer[x][1] -= 1

        # Para cara revisor que nao pode revisar, ele eh adicionado no vetor de exclusao
        for i in xrange(len(self.availableReviewer)):
            if (self.availableReviewer[i][1] <= 0):
                vetDel.append(i)

        # Faz a exclusao dos elementos adcionados na lista de exclusao
        self.availableReviewer = np.delete(self.availableReviewer, vetDel, axis=0)


    # Funcao responsavel por chamar a funcao que faz o "corte" no cromossomo 
    def crossover(self, mate):
        return self.__cross(mate)

    # Funcao que eh responsavel por fazer a multacao em um dos valores do cromossomo
    def mutate(self, index):
        return self.__pick(index)

    # Funcao reponsavel por criar um cromossomo aleatoriamente com base nos revisores disponiveis
    def __makeChromosome(self):
        """
            A ideia geral da funcao de inicializacao, eh fazer com que um cromossomo seja inicalizado
            ja respeitando a integridade do problema, para esta funcao foi ultilizado como base o vetor de
            revisores disponiveis, onde nos sorteamos um elemento no vetor para ser nosso vetor e atribuimos o
            identificador do revisor ao nosso cromossomo, apos adicionar o revisor, nos decrementamos a quantidade
            de artigos que o mesmo pode revisar, fazendo com que caso o revisor nao tenha mais artigos ele seja excluido
            do nosso vetor de revisores disponiveis
        """

        # Cria uma matriz de valores -1 para a criacao da populacao do problema
        chromosome = np.zeros(self.numArticles, dtype=int) - 1

        # # Faz a alocacao dos revisores com os artigos aleatoriamente
        for i in xrange(self.numArticles):
            # Verifica se ainda existem revisores disponiveis para a alocacao dos artigos
            # Esta verificacao eh para caso a expecificao do problema nao seja atendida, fazendo que nao aja revisores para artigos
            if (len(self.availableReviewer) == 0):
                print "Nao eh possivel alocar revisores para os artigos!"
                break
            # Sortea um indice do vetor de revisores
            # Dado o indice ele faz a verificacao se o mesmo pode ser ultilizado
            x = random.randrange(0, len(self.availableReviewer))
            
            # Diminui o numero de artigos que o revisor escolhido ira revisar
            self.availableReviewer[x][POS_NUM_ARTICLE] -= 1

            # Indica qual revisor sera indicado para o artigo
            chromosome[i] = self.availableReviewer[x][POS_REVIWER]
            
            # Caso o revisor chegue em seu limite de revisao ele eh tirado da lista de revisores disponiveis
            if (self.availableReviewer[x][POS_NUM_ARTICLE] == 0):
                # Deleta o revisor do vetor de revisores
                self.availableReviewer = np.delete(self.availableReviewer, [x], axis=0)

        return chromosome


    # Funcao resposavel por armazenar a quantidade de artigos que cada revisor pode revizar
    def __reviewers(self):
        """
            Funcao responsavel por calcular as tuplas do meu vetor de revisores disponiveis
        """
        # Pega a ultima coluna da minha matriz original passada pela entrada
        self.reviewers = self.Matrix[0:self.Matrix.shape[0], self.Matrix.shape[1]-1]

        # Cria um vetor de tuplas contendo o id do revisor e a quantidade de artigos que ele pode revisar
        for i in xrange(len(self.reviewers)):
            self.availableReviewer.append([i, self.reviewers[i]])
        
        # Transforma a tupla criada em um vetor de vetores do numpy
        self.availableReviewer = np.array(self.availableReviewer)

    
    def integrity(self, chromosome):
        """
            Funcao responsavel por validar a integridade de um dado cromossomo
        """

        # Faz uma copia do vetor de revisores para a verificacao de integridade        
        review = np.copy(self.reviewers)
        Flag = True

        # Para cada valor do cromossomo verifica se ele altera a integridade da saida
        for i in chromosome:
            review[i] =  review[i] - 1
            # Caso o numero de artigos ultrapasse o quando um revisor pode avaliar, a flag de avaliacao para false
            if (review[i] < 0):
                Flag = False
                # Sai do laco de verificacao
                break
        # Retorna se a integridade esta mantida no cromossomo
        return Flag

    # Funcao responsavel por fazer a avalicao to estado atual
    def evaluate(self):
        # Faz uma soma de valores na matriz de avaliacao para determinar se o cromosso o pesso desse comossomo
        # Quanto maior o valor retornado melhor foi o resultado da avaliacao
        self.score = np.sum(self.Matrix[self.chromosome, range(len(self.chromosome))])

    # Funcao responsavel por fazer a manipulacao do meu vetor de revisores disponiveis
    def getReviwer(self, x, index):
        # Pega a posicao do revisor na qual ele ira atribuir ao artigo
        reviwer = self.availableReviewer[x][POS_REVIWER]
        reviwerOld = self.chromosome[index]

        # ______________________________________________
        # Faz as configuracores na matriz
        MIndex = -1

        # Faz uma busca na lista de artigos disponiveis para verificar se o revisor tem artigos disponiveis
        for i in xrange(len(self.availableReviewer)):
            if (self.availableReviewer[i][POS_REVIWER] == reviwerOld):
                MIndex = i
                break

        # Caso o revisor foi encontrado
        if (MIndex != -1):
            # Aumenta o numero de artigos que o antigo revisor podia revisar ja que ele deixou de revisar o artigo atual
            self.availableReviewer[MIndex][POS_NUM_ARTICLE] += 1
        else:
            # Adiciona no inicio da lista o revisor que nao estava disponivel na lista de revisores e agora esta
            # O valor adicionado eh uma tupla contendo o id do revisor e um artigo a ser revisado
            self.availableReviewer = np.insert(self.availableReviewer, 0, (reviwerOld, 1), axis=0)

        # Diminui o numero de artigos que o revisor tem disponivies para revisar
        self.availableReviewer[x][POS_NUM_ARTICLE] -= 1

        # Retorna o id do novo revisor que ira revisar o artigo
        return reviwer
    
    def __pick(self, index):

        """
            Funcao responsavel por fazer a mutacao no cromossomo dado um indice passado por parametro
            a ideia geral eh que caso eu tenha revisores disponiveis para a escolha eu atualizo do cromossomo
            como um sorteio dos revisores disponivies, caso eu nao tenho algum revisor disponivel, eu sorteio uma posicao
            aleatoria no meu cromossomo, e apartir do numero obtido eu faco uma permutacao o indice passado e do indice sorteado
            nas posicoes do meu cromossomo
        """

        self.__available()
        # Caso ainda existam revisores disponiveis para a alocacao
        # O mesmo pode ser substituido no indice passado pelo AGs
        if(len(self.availableReviewer) > 0):
            # Pega um revisor aleatoriamente
            x = random.randrange(0,len(self.availableReviewer))

            # Pega o revisor requisitado e arruma o vetor de revisores disponiveis
            reviwer = self.getReviwer(x, index)

            # Faz a troca de revisor na posicao do artigo requisitado
            self.chromosome[index] = reviwer
        # Caso nao seja possivel fazer a troca pelo vetor de revisores disponiveis, entao faz-se uma permutacao no vetor
        # Com o elemento passado junto com um elemento aleatorio no vetor
        else:
            x = random.randrange(0,len(self.chromosome))

            # Faz um swap nas posicoes dos elementos do meu cromossomo
            swap = self.chromosome[index]
            self.chromosome[index] = self.chromosome[x]
            self.chromosome[x] = swap

    # Funcao responsavel por fazer o combinacao do cromossomo
    def __cross(self, other):
        """
            Funcao responsavel por fazer o crossover no cromosso
            A ideia usado para essa funcao foi, sortear uma posicao de corte a apartir da posicao
            sorteada faz uma combinacao dos dois cromossomos passados (No qual o primeiro se refere ao cromossomo da classe interna
            e o segundo cromossomo eh o passado pela funcao).
            Apos fazer a combinacao, ferifica se a integridade eh atendida, caso contrario, eh sortuado um novo
            ponto de corte para refazer o processo, eh importante salientar que caso o ponto de corte seja o valor ZERO
            o cromossomo filho eh uma copia do cromossomo pai
        """

        def mate(P0,P1):            
            # Faz um sorteio para a posicao de corte
            cutoff = random.randint(0, self.numArticles - 1)

            # Cria uma nova copia de P0
            chromosome = np.copy(P0.chromosome[:])
            # Faz o cruzamento dos dois pontos
            chromosome[cutoff:] = P1.chromosome[cutoff:]

            # Caso o cromossomo gerado atenda a integridade proposta pelo problema
            # Gero uma outra posicao de corte e refasso o passo anterior de criacao do filho
            if (not self.integrity(chromosome)):
                flag = True
                while flag:
                    cutoff = random.randint(0, self.numArticles - 1)
                    chromosome = np.copy(P0.chromosome[:])
                    chromosome[:cutoff] = P1.chromosome[:cutoff]
                    # caso a integridade seja atendida sai do laco
                    if (self.integrity(chromosome)):    
                        flag = False

            # Cria um objeto individuo
            child = P0.__class__(chromosome, self.Matrix)
            # Retorna o filho gerado
            return child
            
        return mate(self,other), mate(other,self)


    ########################## HELPERS ############################
    # Funcoes responsaveis apenes para auxilio na hora de fazer operacoes com o objeto da classe

    # Funcao que manimula a representacao do comossomo
    def __repr__(self):
        return '<%s chromosome="%s" score=%s>' % \
        (self.__class__.__name__, STR_DELIMITER.join(str(int(i)) for i in self.chromosome),self.score)

    # Funcao responsavel por mudar como sera feito a comparacao do cromosso
    # Para que a mesma seja ordenada pelo valor da avaliacao do mesmo (score)
    def __cmp__(self,other):
        # Esta comparacao sempre dara prioridade para os maiores valores
        return cmp(other.score, self.score)

    # Funcao responsavel por criar uma replica de si mesma
    def copy(self):
        clone = self.__class__(self.chromosome, self.Matrix)
        clone.score = self.score
        clone.availableReviewer = self.availableReviewer
        return clone

class Article(object):
    
    """
        Classe reponsavel por fazer a manipulacao geral do algoritmo genetico
        Ela tem como objetivo manipular a classe individual, na qual este por sua vez eh o meu objeto base
        do cromossomo

        @param crossoverrate: Taxa de crossover
        @param size: Tamanho da populacao que sera gerada
        @param mutationrate: Taxa de mutacao
        @param maxgen: maximo de geracoes
        @param inputpath: caminho do arquivo da matriz de entrada
        @param interations: numero de iteracoes que meu algoritmos deve fazer para o problema
    """

    def __init__(self, crossoverrate=0.9, size=100, mutationrate=0.01, maxgen=100, inputpath=None, iterations=10, getInformation=True):
        self.size = size
        self.crossrate = crossoverrate
        self.muterate = mutationrate
        self.maxgen = maxgen
        self.inputpath = inputpath
        self.Matrix = None
        self.iterations = iterations

        self.getInformation = getInformation
        self.indexBest = 0

        # Faz a leitura da minha matriz passado pelo parametro inputpath
        self.readFileMatrix()

        # Variavel responsavel por armazenar a minha populacao
        self.population = []

        self.generation = 0
        self.numArticles = self.Matrix.shape[1] - 1

        self.bestsChromosome = []
        # Variavel responsavel por armazenar todos os scores das geracores das iteracores
        self.timeline = []
        # variavel responsavel por armazenar os scores de uma geracao da iteracao atual
        self.realtime = []
        self.otimization = False
        self.timeOut = 0


    # Funcao responsavel por inicializar a populacao de cromossomos
    def __makePopulation(self):
        return [Individual(matrix=self.Matrix) for individual in xrange(self.size)]

    # Funcao responsavel por me dizer se eu cheguei em meu objetivo
    def __goal(self):
        """
            Funcao responsavel por validar se eu terminei as minhas as minhas geracores
            ou se eu cheguei em um resutado otimo

            No caso do resultado otimo eu apenas fiz o inverso da minha funcao de avaliacao
            onde o resultado tentia a um valor proximo ao 5 vezes o numero de artigos,
            entao eu apenas peguei o resultado otimo para o problema e subitrai do fitness do meu
            melhor chromossomo
        """
        return (self.generation > self.maxgen) or (((5*self.numArticles - self.best.score) == 0) and self.otimization)

    # Funcao responsavel por rodar o algoritmo
    def run(self):
        """
            Funcao principal para rodar todo o algoritmo e gerar as minhas saidas
        """
        timeIn = time.time()

        bestIteration = (0,0)
        # Para cada iteracao do problema faz um novo calculo do problema
        for i in xrange(self.iterations):

            # Inicializa uma nova populacao para cada iteracao do algoritmo
            self.population = self.__makePopulation()

            # Para cada individuo criado na populacao calcula o score do mesmo
            for individual in self.population:
                individual.evaluate()

            # Inicializa a geracao sempre com 0
            self.generation = 0

            # Realiza o treinamento para a populacao
            while not self.__goal():
                self.step()

            # Faz uma copia do melhor cromossomo gerado por essa iteracao
            better = self.best.copy()
            better.evaluate()

            # Faz a verificacao de qual foi a melhor iteracao ate o momento
            if (better.score > bestIteration[1]):
                bestIteration = (i, better.score)

            # Envia o cromosso para o meu vetor dos melhor cromossomos de todas as iteracoes
            self.bestsChromosome.append(better)
            # Adiciona os valores da minha iteracao para a minha varial global de socres
            self.timeline.append(self.realtime)

            # Reseta a minha variavel que armazena o valor dos scores da minha iteracao atual
            self.realtime = []

        # Pega o indice da melhor iteracao
        self.indexBest = bestIteration[0]

        # Caso eu queria salvar os dados (Imagens e TXT)
        if (self.getInformation):
            self.graph()

        self.timeOut = time.time() - timeIn

    def step(self):
        # Ordena a minha populacao para que sempre o melhor cromossomo fique no inicio
        # A comparacao eh realizada por meio dos score que cada individuo
        # A comparacao foi alterada na classe dos individuos por meio da funcao __cmp__
        self.population.sort()
        self._crossover()
        self.generation += 1
        # self.report()

        # Adiciona o score do meu melhor cromossomo a minha lista da de realtime
        self.realtime.append(self.best.score)

    # Funcao responsavel por fazer o crossover de cada elemento
    def _crossover(self):
        # Copia o melhor individuo papara o inicio da minha nova populacao
        # Fazendo com que o mesmo percorra o caminho ate o fim caso ele for sempre o melhor
        next_population = [self.best.copy()]
        next_population[0].evaluate()
        # Para cada elemento da minha populacao
        while len(next_population) < self.size:
            # Seleciona um elemento para ser o pai
            father = self._select()
            # Caso eu possa fazer o crossover
            if random.random() < self.crossrate:
                mother = self._select()
                offspring = father.crossover(mother)
            # Caso nao seja feito apenas copia o meu elemento selecionado
            else:
                # Faz uma copia do individuo
                offspring = [father.copy()]
        
            # Para cada individuo copiado faz uma mutacao e adiciona na proxima geracao
            for individual in offspring:
                self._mutate(individual)
                individual.evaluate()
                next_population.append(individual)
        # Copia a proxmima geracao para a populacao
        self.population = next_population[:self.size]

    # Funcao responsavel por fazer a mutacao no cromossomo
    def _mutate(self, individual):
        # Para cara artigo tenta fazer uma mutacao caso o menos atinga a aleatoriedade escolhida no mutetarion rate
        for reviwer in xrange(self.numArticles):
            if random.random() < self.muterate:
                individual.mutate(reviwer)
 
    # Funcao responsavel por fazer a selecao dos individuos
    def _select(self):
        return self._tournament()

    # Funcao responsavel por selecionar os individuos pelo metodo de toneio
    def _tournament(self):
        # Variavel para armazenar os competidores
        competitors = []
        # Faz a soma total dos score normalizando com o score reverso gerado pelo individuo
        total_score = (sum([self.population[i].score for i in xrange(self.size)]))

        # self.size
        for index in xrange(self.size):
            temp = [index] * int(math.ceil(self.population[index].score*100 / total_score))
            # Adiciona o valor do peso do individuo na lista de competidores
            competitors.extend(temp)

        # Faz uma escolha aletoria dos elementos adicionado nos meus competidores
        return self.population[random.choice(competitors)]

    # Funcao auxiliar para mostrar o andamento de cada geracao
    def report(self):
        print "="*70
        print "generation: " , self.generation
        print "best:       " , self.best


    def saveTXT(self):
        saida = ",".join(str(int(i)) for i in self.bestsChromosome[self.indexBest].chromosome)
        saida = saida + "\n"

        try:
            file = open('saida-genetico.txt', 'w')
            file.write(saida)
            file.close()
        except IOError:
            print "Nao foi possivel salvar o arquivo"

    def graph(self):
        
        self.saveTXT()

        # Faz os calculos para a plotagem do grafico
        self.timeline = np.array(self.timeline)
        mean =  np.mean(self.timeline, axis=0)
        bestScore = self.timeline[self.indexBest]

        geracores = range(len(mean)) 

        plt.plot(geracores, mean, label='Media das Solucoes')
        plt.plot(geracores, bestScore, label='Melhor Solucao')
        plt.title('Alocacao de artigos')
        plt.xlabel('Geracores')
        plt.ylabel('Fitness')
        plt.legend()

        plt.savefig("fitness.png")

    # Retorna o melhor fitness entre todas as iteracores
    def getInformations(self):
        # Item 0 = Melhor escore obtido
        # Item 1 = Iteracao que obteve melhor score
        # Item 3 = Tempo total
        # print self.bestsChromosome
        return (self.bestsChromosome[self.indexBest].score, self.indexBest, self.timeOut)

    # Funcao responsavel por ler o arquivo passado e gerar uma matriz numpy
    def readFileMatrix(self):
        # Tenta abrir o arquivo que contem a matriz, caso nao consiga mostra uma mensagem de erro na tela do usuario
        try:
            with open(self.inputpath, 'r') as file:
                # Transforma o texto recebido para uma matriz numpy para uma manipulacao facilitada
                self.Matrix = np.loadtxt(file, delimiter=',', dtype=int)
        except IOError:
            print "Arquivo nao encontrado!"
    
    # Pega o individuo com o melhor fitness score na populacao
    def best():
        def fget(self):
            return self.population[0]
        return locals()
    # Variavel que armazena sempre o melhor individuo da populacao
    best = property(**best())