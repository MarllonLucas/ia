"""
    Integrantes:
    Marllon Lucas Rodrigues Rosa (2017.1904.045-1)
    Eduardo Sobrinho (2018.1904.084-4)
    Mário de Araújo Carvalho (2017.1904.080-0)

"""
from article import Article
import sys

def main():

    inputpath = sys.argv[1]

    # Nao colocar o parametro size maior que 100
    Art = Article(crossoverrate=0.85, size=100, mutationrate=0.01, maxgen=100, inputpath=inputpath)

    Art.run()


if __name__ == '__main__':
    main()
