"""
Integrantes:
    Marllon Lucas Rodrigues Rosa (2017.1904.045-1)
    Eduardo A. P. Sobrinho (2018.1904.084-4)
    Mario de Araujo Carvalho (2017.1904.080-0)

"""

from operator import itemgetter
from scipy.spatial import distance
import copy
import pos
import configFinal

class restaUm:
    inicial = []
    objetivo = configFinal.objetivo

    def __init__(self, estadoInicial):
        self.inicial = estadoInicial

    def son2str(self, s):
        return ''.join([str(v) for v in s])

    def h(self, noTemp, resultado):
        dist = -1
        # Verifica se alguma pedra deste estado esta numa distancia maior que a permitida
        for p in range(0, 7):
            for q in range(0, 7):
                if dist < 4:
                    # Se estiver nas linhas e colunas permitidas dentro do tabuleiro
                    if ((p == 0 or p == 1 or p == 5 or p == 6) and (q == 2 or q == 3 or q == 4)) or p == 2 or p == 3 or p == 4:
                        dist = 128
                        # Compara a distancia da pedra atualmente observada noTemp[p][q] com todas as pedras do tabuleiro noTemp[r][s]
                        for r in range(0, 7):
                            for s in range(0, 7):
                                # Se (a linha for 2 OU 3 OU 4) ou (a coluna for 2 OU 3 OU 4) E noTemp[p][q] E noTemp[r][s] forem 1
                                # Ou seja, se estiver nas linhas e colunas permitidas dentro do tabuleiro
                                if dist > 3 and (p != 0 or (q != 0 and q != 1 and q != 5 and q != 6)) and noTemp[r][s] == 1 and noTemp[p][q] == 1:
                                    distTmp = distance.cityblock([p, q], [r, s])
                                    if (r != p or s != q) and dist > distTmp:
                                        dist = distTmp

                        if dist != 128 and dist > 3:
                            return

        resultado.append(noTemp)
        return resultado

    def sucessores(self, noAtual):
        resultado = []

        # Varre todo o tabuleiro a procura de filhos
        for i in range(0, 7):
            for j in range(0, 7):
                # (Se estiver antes da coluna 5 E entre as linhas 1 e 5) OU (se estiver na coluna 2)
                if (j < 5 and (i > 1 and i < 5)) or j == 2:
                    # Se (i, j) eh zero
                    if noAtual[i][j] == 0:
                        # Se posicoes (i, j-1) e (i, j-2) forem 1
                        # move para esquerda
                        if noAtual[i][j+1] == 1 and noAtual[i][j+2] == 1:
                            noTemp = copy.deepcopy(noAtual)
                            noTemp[i][j+2] = 0
                            noTemp[i][j+1] = 0
                            noTemp[i][j] = 1
                            self.h(noTemp, resultado)

                # (Se estiver acima da linha 5 E entre as colunas 1 e 5) OU (se estiver na linha 2)
                if (i < 5 and (j > 1 and j < 5)) or i == 2:
                    # Se (i, j) eh zero
                    if noAtual[i][j] == 0:
                        # Se posicoes (i-1, j) e (i-2, j) forem 1
                        # move para cima
                        if noAtual[i+1][j] == 1 and noAtual[i+2][j] == 1:
                            noTemp = copy.deepcopy(noAtual)
                            noTemp[i+2][j] = 0
                            noTemp[i+1][j] = 0
                            noTemp[i][j] = 1
                            self.h(noTemp, resultado)

                # (Se estiver depois da coluna 1 E entre as linhas 1 e 5) OU (se estiver na coluna 4)
                if (j > 1 and (i > 1 and i < 5)) or j == 4:
                    # Se (i, j) eh zero
                    if noAtual[i][j] == 0:
                        # Se posicoes (i, j+1) e (i, j+2) forem 1
                        # move para direita
                        if noAtual[i][j-1] == 1 and noAtual[i][j-2] == 1:
                            noTemp = copy.deepcopy(noAtual)
                            noTemp[i][j-2] = 0
                            noTemp[i][j-1] = 0
                            noTemp[i][j] = 1
                            self.h(noTemp, resultado)

                # (Se estiver abaixo da linha 1 E entre as colunas 1 e 5) OU (se estiver na linha 4)
                if (i > 1 and (j > 1 and j < 5)) or i == 4:
                    # Se (i, j) eh zero
                    if noAtual[i][j] == 0:
                        # Se posicoes (i+1, j) e (i+2, j) forem 1
                        # move para baixo
                        if noAtual[i-1][j] == 1 and noAtual[i-2][j] == 1:
                            noTemp = copy.deepcopy(noAtual)
                            noTemp[i-2][j] = 0
                            noTemp[i-1][j] = 0
                            noTemp[i][j] = 1
                            self.h(noTemp, resultado)
        return resultado

    def ehFinal(self,no):
        for i in self.objetivo:
            if i == no:
                return True
        return False

    def aStar(self):
        listaDeEspera = []

        listaDeEspera.append(self.inicial)
        pais = dict()
        visitados = [self.inicial]
        saida = open ('saida-resta-um.txt','w')
        estadosExpandidos = 0
        while len(listaDeEspera) > 0:
            pai = listaDeEspera[0]
            del listaDeEspera[0]
            for filho in self.sucessores(pai):
                if filho not in visitados:
                        visitados.append(filho)
                        pais[self.son2str(filho)] = pai
                        estadosExpandidos += 1
                        if self.ehFinal(filho):
                            resultado = []
                            noTemp = filho
                            while noTemp != self.inicial:
                                resultado.append(noTemp)
                                noTemp = pais[self.son2str(noTemp)]
                            resultado.append(self.inicial)
                            resultado.reverse()
                            saida.writelines('==SOLUCAO\n')
                            estagio = []
                            for i in resultado:
                                for j in i:
                                    estagio.append(j)
                                    if len(estagio) == 14:
                                        jogada = pos.pos(estagio)
                                        if estagio[7:15] == resultado[-1]:
                                            saida.writelines('FINAL ')
                                            saida.writelines(jogada.posicao() + '\n\n')
                                        else:
                                            saida.writelines(jogada.posicao() + '\n')
                                        del estagio[0:7]
                            saida.close()
                            exit(0)

                        else:
                            listaDeEspera.insert(0, filho)

if __name__ == '__main__':
    configuracaoInicial = [[0,0,1,1,1,0,0], [0,0,1,1,1,0,0], [0,0,1,1,1,0,0], [0,0,1,1,1,1,0], [0,0,1,0,0,0,0], [0,0,0,0,0,0,0], [0,0,0,0,0,0,0]]
    r = restaUm(configuracaoInicial)
    r.aStar()
