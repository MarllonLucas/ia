class pos:
    atual = []
    anterior = []

    def __init__(self, estagio):
        self.anterior = estagio[0:7] # matriz da iteracao (jogada) anterior
        self.atual = estagio[7:15]   # matriz da iteracao (jogada) atual

    def posicao(self):
        # Verifica a posicao inicial e a posicao final apos uma jogada
        for i in range(0, 7):
            for j in range(0, 7):
                # (Se estiver abaixo da linha 1 E entre as colunas 1 e 5) OU (se estiver na linha 4)
                if (i > 1 and (j > 1 and j < 5)) or i == 4:
                    # Se posicao (i, j) agora eh 1
                    if self.atual[i][j] == 1:
                        # ... e antes era 0
                        if self.anterior[i][j] == 0:
                            # Entao, se a pedra veio de cima (i-2, j)
                            if self.atual[i-2][j] == 0 and self.anterior[i-2][j] == 1:
                                return '(' + str(i-2) + ', ' + str(j) + ') - (' + str(i) + ', ' + str(j) + ')'

                # (Se estiver antes da coluna 5 E entre as linhas 1 e 5) OU (se estiver na coluna 2)
                if (j < 5 and (i > 1 and i < 5)) or j == 2:
                    # Se posicao (i, j) agora eh 1
                    if self.atual[i][j] == 1:
                        # ... e antes era 0
                        if self.anterior[i][j] == 0:
                            # Entao, se a pedra veio da direita (i, j+2)
                            if self.atual[i][j+2] == 0 and self.anterior[i][j+2] == 1:
                                return '(' + str(i) + ', ' + str(j+2) + ') - (' + str(i) + ', ' + str(j) + ')'

                # (Se estiver acima da linha 5 E entre as colunas 1 e 5) OU (se estiver na linha 2)
                if (i < 5 and (j > 1 and j < 5)) or i == 2:
                    # Se posicao (i, j) agora eh 1
                    if self.atual[i][j] == 1:
                        # ... e antes era 0
                        if self.anterior[i][j] == 0:
                            # Entao, se a pedra veio de baixo (i+2, j)
                            if self.atual[i+2][j] == 0 and self.anterior[i+2][j] == 1:
                                return '(' + str(i+2) + ', ' + str(j) + ') - (' + str(i) + ', ' + str(j) + ')'

                # (Se estiver depois da coluna 1 E entre as linhas 1 e 5) OU (se estiver na coluna 4)
                if (j > 1 and (i > 1 and i < 5)) or j == 4:
                    # Se posicao (i, j) agora eh 1
                    if self.atual[i][j] == 1:
                        # ... e antes era 0
                        if self.anterior[i][j] == 0:
                            # Entao, se a pedra veio da esquerda (i, j-2)
                            if self.atual[i][j-2] == 0 and self.anterior[i][j-2] == 1:
                                return '(' + str(i) + ', ' + str(j-2) + ') - (' + str(i) + ', ' + str(j) + ')'
