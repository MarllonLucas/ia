#!interpreter [optional-arg]
# -*- coding: utf-8 -*-

##################################################
## Este trabalho implementa os algoritmos de classificação:
# Árvore de Decisão,
# Vizinhos Mais Próximos (KNN),
# Naı̈ve Bayes,
# Regressão Logı́stica,
# Redes Neurais MLP.
# Foram utilizados os algoritmos disponı́veis na biblioteca SKLearn.
##################################################
##################################################
## Authors:
# Marllon Lucas Rodrigues Rosa (2017.1904.045-1)
# Eduardo Sobrinho (2018.1904.084-4)
# Mário de Araújo Carvalho (2017.1904.080-0)
##################################################
## Copyright: Copyright 2019, Classification with SKLearn.
## License: Apache 2.0
## Version: 1.19.06
## Email: mario.carvalho@ieee.org
## Status: Testing
##################################################

# Importing auxiliary libraries
import numpy as np
import math
import matplotlib.pyplot as plt
import scipy as sp
import copy # For deep copy

# Importing auxiliary libraries SKLearn
from sklearn import datasets
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.metrics import log_loss

# Importing classifiers
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier

# Classe auxiliar para guardar os resultados de cada item
class MyResults:
	def __init__(self, name, average,  standard_deviation,  folds_accuracy, logistic_loss):
		self.name = name
		self.average = average
		self.standard_deviation = standard_deviation
		self.folds_accuracy = copy.deepcopy(folds_accuracy)
		self.logistic_loss = logistic_loss

# Importing  datasets 
## SKLearn datasets
digits = datasets.load_digits()
ires = datasets.load_iris()
wine = datasets.load_wine()
breast_cancer = datasets.load_breast_cancer()
## UC Irvine Machine Learning Repository datasets (https://archive.ics.uci.edu/ml/index.php)
haberman = np.loadtxt('datasets/data/haberman.csv', usecols = [0,1,2,3], delimiter = ',')
heart = np.loadtxt('datasets/data/heart.csv', usecols = [0,1,2,3,4,5,6,7,8,9,10,11,12,13], delimiter = ',')
tae = np.loadtxt('datasets/data/tae.csv', usecols = [0,1,2,3,4,5], delimiter = ',')
tic_tac_toe = np.loadtxt('datasets/data/tic-tac-toe.csv', usecols = [0,1,2,3,4,5,6,7,8,9], delimiter = ',')
transfusion = np.loadtxt('datasets/data/transfusion.csv', usecols = [0,1,2,3,4], delimiter = ',')
glass = np.loadtxt('datasets/data/glass.csv', usecols = [0,1,2,3,4,5,6,7,8,9], delimiter = ',')

## Túpula de datasets: Name dataset, dados, keyOfClassifier
myDatasets = []
## Populando a túpula com os dados dos datasets
myDatasets.append(('haberman', haberman[0:306, 0:2], haberman[:,-1]))
myDatasets.append(('heart', heart[0:270, 0:12], heart[:,-1]))
myDatasets.append(('tae', tae[0:151, 0:4], tae[:,-1]))
myDatasets.append(('tic_tac_toe', tic_tac_toe[0:958, 0:8], tic_tac_toe[:,-1]))
myDatasets.append(('transfusion', transfusion[0:748, 0:3], transfusion[:,-1]))
myDatasets.append(('glass', glass[0:214, 0:8], glass[:,-1]))
# Datasets SKLearn
myDatasets.append(('digits', digits.data, digits.target))
myDatasets.append(('ires', ires.data, ires.target))
myDatasets.append(('wine', wine.data, wine.target))
myDatasets.append(('breast_cancer', breast_cancer.data, breast_cancer.target))

# Criando os classificadores padrões
myClassifierDT = DecisionTreeClassifier()
myClassifierKNN = KNeighborsClassifier()
myClassifierGNB = GaussianNB() 
myClassifierLR = LogisticRegression()
myClassifierMLP = MLPClassifier()

## Túpula do para armazenar: classificador, parameters, keyOfClassifier
myClassifiers = []
# Populando a túpula com os dados dos classificadores e parâmetros
myClassifiers.append((myClassifierDT, {'max_depth': [5, 15, 19, 25, 35]}, 'myClassifierDT'))
myClassifiers.append((myClassifierKNN, {'n_neighbors': [45, 10, 20, 40, 50]}, 'myClassifierKNN'))
myClassifiers.append((myClassifierGNB, {'var_smoothing': [1, 1e-05, 1e-06, 1e-07]}, 'myClassifierGNB'))
myClassifiers.append((myClassifierLR, {'random_state': [10,25,50,55,60]}, 'myClassifierLR'))
myClassifiers.append((myClassifierMLP, {'max_iter': [500, 350, 600, 500, 300]}, 'myClassifierMLP'))

# Minha lista de resultados para armazenar os itens dos resultados e cada classificador
myListResults = []

# Para cada classificador
for i in range( len(myClassifiers) ):
	classificador, parameters, name = myClassifiers[i]
	
	# GridSearch
	gd = GridSearchCV(estimator = classificador, param_grid = parameters, scoring = 'accuracy', cv = 10, n_jobs = -1)
	
	# Lista para inserir os elementos para a class result
	myItemListResult = []

	# Executar o classificador corrente para a lista de datasets 
	for j in range( len(myDatasets) ):
		# Pegando os dados da túpula
		nameDataset, dataDataset, targetDataset = myDatasets[j]
		# Melhores paramêtros (best_params)
		bp = gd.fit(dataDataset, targetDataset)
		# Verificar qual o classificador corrente
		if name == 'myClassifierDT':
			# Pegando os melhores parâmetros
			classificador = DecisionTreeClassifier(max_depth = bp.best_params_['max_depth'])
		elif name == 'myClassifierKNN':
			# Pegando os melhores parâmetros
			classificador = KNeighborsClassifier(n_neighbors = bp.best_params_['n_neighbors'])
		elif name == 'myClassifierGNB':
			classificador = GaussianNB()
		elif name == 'myClassifierLR':
			# Pegando os melhores parâmetros
			classificador = LogisticRegression(random_state = bp.best_params_['random_state'])
		elif name == 'myClassifierMLP':
			# Pegando os melhores parâmetros
			classificador = MLPClassifier(max_iter = bp.best_params_['max_iter'])

		# Calculando os resultados finais
		# Pegando a acurácia
		acuracia = cross_val_score(classificador, dataDataset, targetDataset, cv=10)
		# Pegando a média
		average = np.mean(acuracia)
		# Pegando o desvio padrão
		standard_deviation = math.sqrt(acuracia.std())
		# Retreinando com os melhores parâmetros
		classificador.fit(dataDataset,targetDataset)
		# Pegando a probabilidade
		probability = classificador.predict_proba(dataDataset)
		# Perda logística
		logistic_loss = log_loss(targetDataset, probability)
		# Insert in item list
		myItemListResult.append(MyResults(nameDataset, average,  standard_deviation, acuracia, logistic_loss))
	# Insert in list results
	myListResults.append((name, myItemListResult))

# Pegando os resultados final
for i in range( len(myListResults) ):
	name, results = myListResults[i]
	print('-------------------------------------------------------------------\n')
	print ('Classifier: ' + name)
	for j in results:
		print ('Dataset: ' + j.name)
		print ('Media: %.5f' % j.average)
		print ('Desvio padrao: %.5f' % j.standard_deviation)
		print ('Perda logistica: %.5f' % j.logistic_loss)
		print ('Acuracia:', j.folds_accuracy)