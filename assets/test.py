#!interpreter [optional-arg]
# -*- coding: utf-8 -*-

##################################################
## Este trabalho implementa os algoritmos de classificação:
# Árvore de Decisão,
# Vizinhos Mais Próximos (KNN),
# Naı̈ve Bayes,
# Regressão Logı́stica,
# Redes Neurais MLP.
# Foram utilizados os algoritmos disponı́veis na biblioteca SKLearn.
##################################################
##################################################
## Authors:
# Marllon Lucas Rodrigues Rosa (2017.1904.045-1)
# Eduardo Sobrinho (2018.1904.084-4)
# Mário de Araújo Carvalho (2017.1904.080-0)
##################################################
## Copyright: Copyright 2019, Classification with SKLearn.
## License: Apache 2.0
## Version: 1.19.06
## Email: mario.carvalho@ieee.org
## Status: Testing
##################################################

# Importing auxiliary libraries
import numpy as np
import math
import matplotlib.pyplot as plt
import scipy as sp
import copy # For deep copy

# Importing auxiliary libraries SKLearn
from sklearn import datasets
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.metrics import log_loss

# Importing classifiers
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier


class Dataset:
	def __init__(self, name, data, target):
		self.name = copy.deepcopy(name)
		self.data = copy.deepcopy(data)
		self.target = copy.deepcopy(target)

class Resultados:
	def __init__(self, media, desvio_padrao, acuraciaFolds, log_loss):
		self.media = media
		self.desvio_padrao = desvio_padrao
		self.acuraciaFolds = copy.deepcopy(acuraciaFolds)
		self.log_loss = log_loss

class Oin:
	def __init__(self, name, lista):
		self.name = name # Nome do classificador
		self.listaResulados = lista

# ---------------------------------------------------------------------------------------------------------------------

# Importing  datasets 
## SKLearn datasets
digits = datasets.load_digits()
ires = datasets.load_iris()
wine = datasets.load_wine()

## UC Irvine Machine Learning Repository datasets (https://archive.ics.uci.edu/ml/index.php)

balance_scale = np.loadtxt('datasets/data/balance-scale.csv', usecols = [0,1,2,3,4], delimiter = ',')
haberman = np.loadtxt('datasets/data/haberman.csv', usecols = [0,1,2,3], delimiter = ',')
heart = np.loadtxt('datasets/data/heart.csv', usecols = [0,1,2,3,4,5,6,7,8,9,10,11,12,13], delimiter = ' ')
tae = np.loadtxt('datasets/data/tae.csv', usecols = [0,1,2,3,4,5], delimiter = ',')
tic_tac_toe = np.loadtxt('datasets/data/tic-tac-toe.csv', usecols = [0,1,2,3,4,5,6,7,8,9], delimiter = ',')
transfusion = np.loadtxt('datasets/data/transfusion.csv', usecols = [0,1,2,3,4], delimiter = ',')
glass = np.loadtxt('datasets/data/glass.csv', usecols = [0,1,2,3,4,5,6,7,8,9,10], delimiter = ',')

print('ok')
