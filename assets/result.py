#!interpreter [optional-arg]
# -*- coding: utf-8 -*-

##################################################
## Este trabalho implementa os algoritmos de classificação:
# Árvore de Decisão,
# Vizinhos Mais Próximos (KNN),
# Naı̈ve Bayes,
# Regressão Logı́stica,
# Redes Neurais MLP.
# Foram utilizados os algoritmos disponı́veis na biblioteca SKLearn.
##################################################
##################################################
## Authors:
# Marllon Lucas Rodrigues Rosa (2017.1904.045-1)
# Eduardo Sobrinho (2018.1904.084-4)
# Mário de Araújo Carvalho (2017.1904.080-0)
##################################################
## Copyright: Copyright 2019, Classification with SKLearn.
## License: Apache 2.0
## Version: 1.19.06
## Email: mario.carvalho@ieee.org
## Status: Testing
##################################################

# Importing auxiliary libraries
import copy # For deep copy

class MyResults:
	def __init__(self, name, average,  standardDeviation,  foldsAccuracy, logistic_loss):
		self.name = name
		self.average = average
		self.standardDeviation =  standardDeviation
		self.foldsAccuracy = copy.deepcopy( foldsAccuracy)
		self.logistic_loss = logistic_loss