#!interpreter [optional-arg]
# -*- coding: utf-8 -*-

##################################################
## Este trabalho implementa os algoritmos de classificação:
# Árvore de Decisão,
# Vizinhos Mais Próximos (KNN),
# Naı̈ve Bayes,
# Regressão Logı́stica,
# Redes Neurais MLP.
# Foram utilizados os algoritmos disponı́veis na biblioteca SKLearn.
##################################################
##################################################
## Authors:
# Marllon Lucas Rodrigues Rosa (2017.1904.045-1)
# Eduardo Sobrinho (2018.1904.084-4)
# Mário de Araújo Carvalho (2017.1904.080-0)
##################################################
## Copyright: Copyright 2019, Classification with SKLearn.
## License: Apache 2.0
## Version: 1.19.06
## Email: mario.carvalho@ieee.org
## Status: Testing
##################################################

# Importing auxiliary libraries
import numpy as np
import math
import matplotlib.pyplot as plt
import scipy as sp
import copy # For deep copy

# Importing auxiliary libraries SKLearn
from sklearn import datasets
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.metrics import log_loss

# Importing classifiers
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier

class Resultados:
	def __init__(self, name, media, desvio_padrao, acuraciaFolds, log_loss):
		self.name = name
		self.media = media
		self.desvio_padrao = desvio_padrao
		self.acuraciaFolds = copy.deepcopy(acuraciaFolds)
		self.log_loss = log_loss

class Oin:
	def __init__(self, name, lista):
		self.name = name # Nome do classificador
		self.listaResulados = lista

# Importing  datasets 

## SKLearn datasets
digits = datasets.load_digits()
ires = datasets.load_iris()
wine = datasets.load_wine()
breast_cancer = datasets.load_breast_cancer()
## UC Irvine Machine Learning Repository datasets (https://archive.ics.uci.edu/ml/index.php)
balance_scale = np.loadtxt('datasets/data/balance-scale.csv', usecols = [0,1,2,3,4], delimiter = ',')
haberman = np.loadtxt('datasets/data/haberman.csv', usecols = [0,1,2,3], delimiter = ',')
heart = np.loadtxt('datasets/data/heart.csv', usecols = [0,1,2,3,4,5,6,7,8,9,10,11,12,13], delimiter = ',')
tae = np.loadtxt('datasets/data/tae.csv', usecols = [0,1,2,3,4,5], delimiter = ',')
tic_tac_toe = np.loadtxt('datasets/data/tic-tac-toe.csv', usecols = [0,1,2,3,4,5,6,7,8,9], delimiter = ',')
transfusion = np.loadtxt('datasets/data/transfusion.csv', usecols = [0,1,2,3,4], delimiter = ',')
glass = np.loadtxt('datasets/data/glass.csv', usecols = [0,1,2,3,4,5,6,7,8,9], delimiter = ',')

## Túpula de datasets: Name dataset, dados, keyOfClassifier
myDatasets = []
# Populando a túpula com os dados dos datasets
# myDatasets.append(('balance_scale', balance_scale[0:625, 0:3], balance_scale[:,-1]))
# myDatasets.append(('haberman', haberman[0:306, 0:2], haberman[:,-1]))
# myDatasets.append(('heart', heart[0:270, 0:12], heart[:,-1]))
# myDatasets.append(('tae', tae[0:151, 0:4], tae[:,-1]))
# myDatasets.append(('tic_tac_toe', tic_tac_toe[0:958, 0:8], tic_tac_toe[:,-1]))
# myDatasets.append(('transfusion', transfusion[0:748, 0:3], transfusion[:,-1]))
# myDatasets.append(('glass', glass[0:214, 0:8], glass[:,-1]))

myDatasets.append(('digits', digits.data, digits.target))
# myDatasets.append(('ires', ires.data, ires.target))
# myDatasets.append(('wine', wine.data, wine.target))
# myDatasets.append(('breast_cancer', breast_cancer.data, breast_cancer.target))

# Criando os classificadores padrões
myClassifierDT = DecisionTreeClassifier()
myClassifierKNN = KNeighborsClassifier()
myClassifierGNB = GaussianNB() 
myClassifierLR = LogisticRegression()
myClassifierMLP = MLPClassifier()

## Túpula do para armazenar: classificador, parameters, keyOfClassifier
myClassifiers = []
# Populando a túpula com os dados dos classificadores e parâmetros
myClassifiers.append((myClassifierDT, {'max_depth': [5, 15, 19, 25, 35]}, 'myClassifierDT'))
myClassifiers.append((myClassifierKNN, {'n_neighbors': [45, 10, 20, 40, 50]}, 'myClassifierKNN'))
myClassifiers.append((myClassifierGNB, {'var_smoothing': [1, 1e-05, 1e-06, 1e-07]}, 'myClassifierGNB'))
myClassifiers.append((myClassifierLR, {'random_state': [10,25,50,55,60]}, 'myClassifierLR'))
myClassifiers.append((myClassifierMLP, {'max_iter': [40, 50, 100, 500, 750]}, 'myClassifierMLP'))


lista = [] # Cada elemento eh da classe Oin

 # Andando pela lista de (5) classificadores
for i in range( len(myClassifiers) ):
	# Pegando objeto classificador, sua combinacao de 5 parameters e seu nome
	classificador, parameters, name = myClassifiers[i]
	
	# Aplicando GridSearch
	gd = GridSearchCV(estimator = classificador, param_grid = parameters, scoring = 'accuracy', cv = 10, n_jobs = -1)
	
	# Cada elemento eh da classe Resultados
	listaResulados = [] 
	
	# Percorrendo lista de datasets
	for j in range( len(myDatasets) ):
		nameDataset, dataDataset, targetDataset = myDatasets[j]
		bp = gd.fit(dataDataset, targetDataset)

		if name == 'myClassifierDT':
			# Retreinando o classificador com os melhores parameters para um dataset especifico
			classificador = DecisionTreeClassifier(max_depth = bp.best_params_['max_depth'])

			acuracia = cross_val_score(classificador, dataDataset, targetDataset, cv=10)
			media = np.mean(acuracia)
			desvio_padrao = math.sqrt(acuracia.std())


			classificador.fit(dataDataset,targetDataset)
			prob_clf = classificador.predict_proba(dataDataset)
			logistic_loss = log_loss(targetDataset, prob_clf)

		elif name == 'myClassifierKNN':
			classificador = KNeighborsClassifier(n_neighbors = bp.best_params_['n_neighbors'])

			acuracia = cross_val_score(classificador, dataDataset, targetDataset, cv=10)
			media = np.mean(acuracia)
			desvio_padrao = math.sqrt(acuracia.std())

			classificador.fit(dataDataset,targetDataset)
			prob_clf = classificador.predict_proba(dataDataset)
			logistic_loss = log_loss(targetDataset, prob_clf)

		elif name == 'myClassifierGNB':
			classificador = GaussianNB()

			acuracia = cross_val_score(classificador, dataDataset, targetDataset, cv=10)
			media = np.mean(acuracia)
			desvio_padrao = math.sqrt(acuracia.std())

			classificador.fit(dataDataset,targetDataset)
			prob_clf = classificador.predict_proba(dataDataset)
			logistic_loss = log_loss(targetDataset, prob_clf)

		elif name == 'myClassifierLR':
			classificador = LogisticRegression(random_state = bp.best_params_['random_state'])

			acuracia = cross_val_score(classificador, dataDataset, targetDataset, cv=10)
			media = np.mean(acuracia)
			desvio_padrao = math.sqrt(acuracia.std())

			classificador.fit(dataDataset,targetDataset)
			prob_clf = classificador.predict_proba(dataDataset)
			logistic_loss = log_loss(targetDataset, prob_clf)

		elif name == 'myClassifierMLP':
			classificador = MLPClassifier(max_iter = bp.best_params_['max_iter']) 

			acuracia = cross_val_score(classificador, dataDataset, targetDataset, cv=10)
			media = np.mean(acuracia)
			desvio_padrao = math.sqrt(acuracia.std())

			classificador.fit(dataDataset,targetDataset)
			prob_clf = classificador.predict_proba(dataDataset)
			logistic_loss = log_loss(targetDataset, prob_clf)

		# Colocando na lista de Resulados
		listaResulados.append(Resultados(nameDataset, media, desvio_padrao, acuracia, logistic_loss))


	lista.append(Oin(name, listaResulados))


for i in lista:
	print('-------------------------------------------------------------------\n')
	print ('\n Classifier: ' + i.name)
	for j in i.listaResulados:
		# print ('\n media = %.5f' % j.media, ' desvio_padrao = %.5f' % j.desvio_padrao, 'log_loss = %.5f' % j.log_loss, '\nacuraria = ', j.acuraciaFolds)
		print ('\n Dataset: ' + j.name)
		print ('\n Media: ' + j.media)
   	print('-------------------------------------------------------------------\n')

		# print ('\n media = %.5f' % j.media, ' desvio_padrao = %.5f' % j.desvio_padrao, 'log_loss = %.5f' % j.log_loss, '\nacuraria = ', j.acuraciaFolds)